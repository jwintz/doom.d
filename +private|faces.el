;;; +private|faces.el -*- lexical-binding: t; no-byte-compile: t; -*-

(require '+private|themes)
(require 'solaire-mode)

;; (setq default-font "Cartograph CF")
;; (setq default-font "Operator Mono")
(setq default-font "Ubuntu Mono")

(when IS-MAC
  (setq default-font-size 11.0)
  (setq default-nice-size 10.0))

(when IS-LINUX
  (setq default-font-size 13.0)
  (setq default-nice-size 13.0))

(setq doom-font-increment 1)
(setq doom-font (font-spec :family default-font :size default-font-size))
(setq doom-variable-pitch-font (font-spec :family "Recoleta" :size default-nice-size))
(setq doom-unicode-font (font-spec :family default-font :size default-font-size))
(setq doom-serif-font (font-spec :family "Recoleta" :size default-nice-size))
(setq doom-themes-enable-bold t)
(setq doom-themes-enable-italic t)

(add-to-list 'solaire-mode-themes-to-face-swap '+private|theme-light)
(add-to-list 'solaire-mode-themes-to-face-swap '+private|theme-mono-light)

(defun +private/palette-get (name)
  "Return hex value of color in private-current-palette by NAME"
  (let ((val (car-safe (map-elt private-current-palette name 'missing))))
    (if (and (not (null val))
             (not (eql val 'missing))
             (not (stringp val)))
        (+private/palette-get val)
      val)))


(defface +private|faces-bufferid   '((t . (:inherit font-lock-builtin-face :family "Operator Mono" :weight bold :height 1.0))) "" :group '+private-faces)

(defface +private|faces-objed-mode '((t (:foreground "#000000"))) "" :group 'private-faces)
(defface +private|faces-emacs-mode '((t (:foreground "#000000"))) "" :group 'private-faces)
(defface +private|faces-keybinding '((t (:foreground "#000000"))) "" :group 'private-faces)
(defface +private|faces-section    '((t . (:inherit font-lock-keyword-face :family "Operator Mono" :weight bold :height 1.0))) "" :group '+private-faces)
(defface +private|faces-hint       '((t . (:inherit font-lock-builtin-face))) "" :group '+private-faces)

(defun +private/faces-update-a (theme &rest _)
;; (set-face-attribute 'mode-line nil :height 120)
  (set-face-attribute '+private|faces-objed-mode nil :foreground (+private/palette-get 'evil-emacs))
  (set-face-attribute '+private|faces-emacs-mode nil :foreground (+private/palette-get 'evil-normal))
  (set-face-attribute '+private|faces-keybinding nil
                      :foreground (+private/palette-get 'black0)
                      :background (+private/palette-get 'purple3)
                      :height 1.4
                      :family "Roboto Mono")

  ;; (require 'highlight-indent-guides)

  ;; (set-face-attribute 'highlight-indent-guides-character-face nil :foreground (color-lighten-name (face-attribute 'default :background) 50))

  (if (or
       (string= theme "+private|theme-light")
       (string= theme "+private|theme-mono-light"))
    (set-face-attribute 'line-number nil :background (face-attribute 'default :background))))

(advice-add #'load-theme :after #'+private/faces-update-a)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-faces
 '(org-document-title ((t (:height 2.0))))
 '(org-level-1 ((t (:height 1.75))))
 '(org-level-2 ((t (:height 1.5))))
 '(org-level-3 ((t (:height 1.25))))
 '(org-level-4 ((t (:height 1.1)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|faces)
