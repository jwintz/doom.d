;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(add-to-list              'load-path doom-private-dir)
(add-to-list 'custom-theme-load-path doom-private-dir)

(require '+private|core)
(require '+private|completion)
(require '+private|defaults)
(require '+private|layout)
(require '+private|editor)
(require '+private|lang|cc)
(require '+private|lang|json)
(require '+private|lang|org)
(require '+private|lang|python)
(require '+private|lang|qml)
(require '+private|lang|rust)
(require '+private|lang|sh)
(require '+private|lang|web)
(require '+private|linting)
(require '+private|vcs)
(require '+private|shell)
(require '+private|faces)
(require '+private|modeline)
(require '+private|modal|guide)
(require '+private|modal|tutorial)
(require '+private|windows)
(require '+private|popups)
(require '+private|themes)
(require '+private|ux)
;; (require '+private|modal)
(require '+private|lookup)
(require '+private|bindings)
