;;; +private|popups.el -*- lexical-binding: t; -*-

(defvar +private|popup-buffer-name "*Private Popup*" "")

(defvar +private|popup-mode-map
  (let ((map (make-sparse-keymap)))
    ;; (define-key map (kbd "q") #'+private|popup-leave)
    ;; (define-key map (kbd "k") #'+private|popup-kill)
    map)
  "")

(define-derived-mode +private|popup-mode +popup-mode "Private Popup"
  "")

(defun +private|popup/toggle ()
  (interactive)
  (if-let (win (get-buffer-window +private|popup-buffer-name))
      (delete-window win)
    (let ((buffer (get-buffer-create +private|popup-buffer-name)))
      (with-current-buffer buffer
        (unless (eq major-mode '+private|popup-mode)
          (+private|popup-mode)
          (+private|popup-setup-buffer))
        (+private|popup-update-buffer))
      (pop-to-buffer buffer)))
  (get-buffer +private|popup-buffer-name))

(defun +private|popup-setup-buffer ()
  "Update the current buffer contents."
  (interactive)
  (+private|popup--save-point
   (+private|popup--with-writable-buffer
    (delete-region (point-min) (point-max))
    (+private|popup-update-buffer))))

(defun +private|popup-update-buffer ()
  "Update the current buffer contents."
  (message "'#+private|popup-update-buffer"))

(defmacro +private|popup--save-point (&rest body)
  "Evaluate BODY and restore the point.
Similar to `save-excursion' but only restore the point."
  (declare (indent 0) (debug t))
  (let ((point (make-symbol "point")))
    `(let ((,point (point)))
       ,@body
       (goto-char (min ,point (point-max))))))

(defmacro +private|popup--with-writable-buffer (&rest body)
  "Evaluate BODY as if the current buffer was not in `read-only-mode'."
  (declare (indent 0) (debug t))
  `(let ((inhibit-read-only t))
     ,@body))

(set-popup-rule! +private|popup-buffer-name :side 'right :size 0.25 :select t :quit nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|popups)
