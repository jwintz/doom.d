;;; +private|windows.el -*- lexical-binding: t; -*-

(defcustom windmove-pre-move-hook nil
  "Hook run before windmove select is triggered."
  :group 'windmove
  :type 'hook)

(defcustom windmove-post-move-hook nil
  "Hook run after windmove select is triggered."
  :group 'windmove
  :type 'hook)

(defadvice windmove-do-window-select (around my-windmove-do-window-select act)
  (run-hooks 'windmove-pre-move-hook)
  ad-do-it
  (run-hooks 'windmove-post-move-hook))

(defun +private/windows-enter-maybe ()
  ;; (if mode-line-format
  ;;     (setq-default header-line-format mode-line-format
  ;;                   mode-line-format nil))
  )

(add-hook! 'prog-mode-hook #'+private/windows-enter-maybe)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|windows)
