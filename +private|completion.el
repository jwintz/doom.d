;;; +private|completion.el -*- lexical-binding: t; -*-

(after! company
  (setq-default company-frontends '(company-preview-frontend company-echo-metadata-frontend)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|completion)
