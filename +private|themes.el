;;; private-themes.el --- A set of eye pleasing themes  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2021 ogdenwebb

;; Author: Ogden Webb <ogdenwebb@gmail.com>
;; URL: https://github.com/ogdenwebb/emacs-private-themes
;; Package-Requires: ((emacs "25.1") (autothemer "0.2.2") (cl-lib "0.6"))
;; Version: 1.6.6
;; Keywords: dark light teal blue violet purple brown theme faces

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Kaolin is a set of eye pleasing themes for GNU Emacs
;; With support a large number of modes and external packages.
;; Kaolin themes are based on the pallete that was originally
;; inspired by Sierra.vim with adding some extra colors.
;;
;; -------  This package includes the following themes  -------
;;
;;  * private-dark - a dark jade variant inspired by Sierra.vim.
;;  * private-light - light variant of the original private-dark.
;;  * private-aurora - Kaolin meets polar lights.
;;  * private-bubblegum - Kaolin colorful theme with dark blue background.
;;  * private-eclipse - a dark purple variant.
;;  * private-galaxy - bright theme based on one of the Sebastian Andaur arts.
;;  * private-mono-dark - almost monochrome dark green Kaolin theme.
;;  * private-mono-light - light variant of monochrome theme.
;;  * private-ocean - dark blue variant.
;;  * private-shiva - theme with autumn colors and melanzane background.
;;  * private-temple - dark brown background with syntax highlighting based on orange and cyan shades.
;;  * private-valley-dark - colorful Kaolin theme with brown background.
;;  * private-valley-light - light version of private-valley-dark theme.
;;
;;
;; -------  Configuration example  -------
;;
;;  (require 'private-themes)
;;  (load-theme 'private-dark t)
;;
;;  ;; Apply treemacs customization for Kaolin themes, requires the all-the-icons package.
;;  (private-treemacs-theme)

;;  ;; Or if you have use-package installed
;;  (use-package private-themes
;;    :config
;;    (load-theme 'private-dark t)
;;    (private-treemacs-theme))
;;
;;  ;;  Custom theme settings
;;
;;  ;; The following set to t by default
;;  (setq private-themes-bold t       ; If nil, disable the bold style.
;;        private-themes-italic t     ; If nil, disable the italic style.
;;        private-themes-underline t) ; If nil, disable the underline style.
;;
;; -------  Some extra theme features, disabled by default  -------
;;
;;  ;; If t, use the wave underline style instead of regular underline.
;;  (setq private-themes-underline-wave t)
;;
;;  ;; When t, will display colored hl-line style
;;  (setq private-themes-hl-line-colored t)
;;
;;
;;; Code:

(eval-when-compile
  (require 'cl-lib))

(require 'autothemer)
(require 'map)
(require 'color)

(require '+private|theme-lib)

(defgroup private-themes nil
  "Kaolin theme properties."
  :group 'faces)

(defcustom private-themes-bold t
  "If nil, disable the bold style."
  :group 'private-themes)

(defcustom private-themes-italic t
  "If nil, disable the italic style."
  :group 'private-themes)

(defcustom private-themes-underline t
  "If nil, disable the underline style."
  :group 'private-themes)

(defcustom private-themes-underline-wave t
  "When t, use the wave underline style to highlight warnings and error."
  :group 'private-themes)

(defcustom private-themes-hl-line-colored nil
  "When t, will display colored hl-line style instead dim gray."
  :group 'private-themes)

(defcustom private-theme-linum-hl-line-style nil
  "When t, enable same style for hl-line and line number faces.")

(defcustom private-themes-italic-comments nil
  "If t, enable italic style in comments."
  :group 'private-themes)

(defcustom private-themes-comments-style 'normal
  "Sets the style of commentaries: normal which is used by default, alt to use colored commentary or contrast to make them more distinguished."
  :options '(normal alt contrast)
  :group 'private-themes)

(defcustom private-themes-git-gutter-solid nil
  "If t, display solid line to highlight git-gutter changes in fringe."
  :group 'private-themes)

(defcustom private-themes-distinct-fringe nil
  "Enable distinct background for fringe and line numbers."
  :group 'private-themes)

(defcustom private-themes-distinct-company-scrollbar nil
  "Enable distinct colors for company popup scrollbar."
  :group 'private-themes)

(defcustom private-themes-distinct-parentheses nil
  "Enable distinct colors for parentheses (i.e. rainbow delimiters package)."
  :group 'private-themes)

(defcustom private-themes-org-scale-headings t
  "If not-nil, scale heading size in org-mode."
  :group 'private-themes)

(defcustom private-themes-modeline-border t
  "If not-nil, enable distinct border in mode-line."
  :group 'private-themes)

(defcustom private-themes-modeline-padded nil
  "Add extra padding for mode-line. Should be either nil/t or desired integer value. It has prior over `private-themes-modeline-border'."
  :group 'private-themes
  :type '(choice integer boolean))

(defcustom private-themes-distinct-metakeys t
  "If not-nil, enable distinct color for metadata key (e.g. metakeys in org-mode).
Otherwise inherit from comments."
  :group 'private-themes)

(defface private-themes-boolean nil
  "Face to highlight boolean values"
  :group 'private-themes)

(define-obsolete-variable-alias 'private-bold 'private-themes-bold "1.3.4")
(define-obsolete-variable-alias 'private-italic 'private-themes-italic "1.3.4")
(define-obsolete-variable-alias 'private-underline 'private-themes-underline "1.3.4")
(define-obsolete-variable-alias 'private-wave 'private-themes-underline-wave "1.3.4")
(define-obsolete-variable-alias 'private-hl-line-colored 'private-themes-hl-line-colored "1.3.4")
(define-obsolete-variable-alias 'private-italic-comments 'private-themes-italic-comments "1.3.4")
(define-obsolete-variable-alias 'private-git-gutter-solid 'private-themes-git-gutter-solid "1.3.4")
(define-obsolete-variable-alias 'private-wave 'private-themes-underline-wave "1.3.4")

(defun private-themes--make-name (sym)
  "Format private-<sym> from SYM."
  (intern (format "+private|theme-%s" (symbol-name sym))))

(defun private-themes--merge-alist (base-alist add-alist)
  "Add elements to BASE-LIST from ADD-LIST without dublicates. Returns a new list as result."
  (let ((res (copy-alist base-alist)))
    (cl-loop for el in add-alist
                    do (setf (map-elt res (car el)) (cdr el)))
    res))

(defvar private-current-palette)

(defmacro define-private-theme (name doc &optional opt-palette opt-faces &rest body)
  "Define new Kaolin theme, using NAME as part of full private-<name>,
the DOC argument provides a short description for new theme.

OPT-PALETTE is a list marks a optional theme palette which will be merged with the `private-palette',
and OPT-FACES is a list for new theme faces. Any color defined within OPT-PALETTE
will override the original one,similar with faces from OPT-PALETTE and `private-faces'.
In terms of private-themes GNU Emacs, palette contains both colors
(such as blue1, orange2 and etc) and variables(bg1, var, functions, etc)
to highlight specific part of GNU Emacs.

Palette is a ordinary association list, e.g. ((color1 \"#ffffff\") (color2 \"#ff0000\")).
You can define your own color/variable (my-own-red \"#ff0000\") in HEX
or inherit a value from another variable (my-own-color red3).

Use private-valley-dark-theme.el as example."
  (let* ((private-theme-name (private-themes--make-name name))
         (private-theme-palette (if opt-palette
                                   (private-themes--merge-alist private-palette opt-palette)
                                 private-palette))
         (private-theme-faces (if opt-faces
                                   (private-themes--merge-alist private-faces opt-faces)
                               private-faces)))

    (setq private-current-palette private-theme-palette)

    `(autothemer-deftheme ,private-theme-name ,doc

                          ((((class color) (min-colors #xFFFFFF)) ; 24bit gui
                            ((class color) (min-colors #xFF))     ; 256
                            t)                                    ; tty

                           ;; Set palette
                           ,@private-theme-palette)

                          ;; Set faces
                          ,private-theme-faces

                          ;; Set vars or execute an arbitrary function body
                           ,@body

                           ;; (custom-theme-set-faces ',private-theme-name
                           ;;                         ,@private-common-vars)


                           (custom-theme-set-variables ',private-theme-name
                              `(ansi-color-names-vector [,private-black
                                                         ,private-red
                                                         ,private-green
                                                         ,private-yellow
                                                         ,private-blue
                                                         ,private-magenta
                                                         ,private-cyan
                                                         ,private-white])
                               `(pos-tip-background-color ,tooltip-bg)
                               `(pos-tip-foreground-color ,tooltip-fg))

                           ;; Provide theme
                           (provide-theme ',private-theme-name)
                           )))


;;;###autoload
(defun private-treemacs-theme ()
  "Enable private-themes treemacs theme with all-the-icons package."
  (require '+private|theme-treemacs))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (let* ((base (file-name-directory load-file-name))
         (dir (expand-file-name "themes/" base)))
    (add-to-list 'custom-theme-load-path
                 (or (and (file-directory-p dir) dir)
                     base))))

(provide '+private|themes)

;;; private-themes.el ends here
