;;; +private|bindings.el -*- lexical-binding: t; -*-

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Restore emacs' defaults
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(map! "C-a" 'beginning-of-line)
(map! "C-e" 'end-of-line)
(map! "C-+" 'text-scale-increase)
(map! "C--" 'text-scale-decrease)
(map! "M-g o" nil)
(map! "M-g" 'objed-activate)

(when (and (display-graphic-p) IS-MAC)
  (map! "s-<up>" 'windmove-up)
  (map! "s-<down>" 'windmove-down)
  (map! "s-<left>" 'windmove-left)
  (map! "s-<right>" 'windmove-right))

(when (and (display-graphic-p) IS-LINUX)
  (map! "M-<up>" 'windmove-up)
  (map! "M-<down>" 'windmove-down)
  (map! "M-<left>" 'windmove-left)
  (map! "M-<right>" 'windmove-right))

(when (not (display-graphic-p))
  (map! "C-x <up>" 'windmove-up)
  (map! "C-x <down>" 'windmove-down)
  (map! "C-x <left>" 'windmove-left)
  (map! "C-x <right>" 'windmove-right))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Leader: C-c in emacs mode
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(map! :leader "e" nil)
(map! :leader
      (:prefix-map ("e" . "editor")
       :desc "Make box comment"  "c"   #'make-box-comment
       :desc "Make div comment"  "d"   #'make-divider
       :desc "Make header"       "m"   #'make-header
       :desc "Simultaneous edit" "i"   #'iedit-mode
       :desc "Switch file"       "s"   #'ff-find-other-file)
      (:prefix-map ("v" . "versioning")
       :desc "Magit"            "v"   #'magit)
      (:prefix-map ("w" . "windows")
       :desc "Hydra"            "h"   #'+hydra/window-nav/body)
      (:prefix-map ("n" . "org")
       :desc "" "l" #'org-roam-buffer-toggle
       :desc "" "f" #'org-roam-node-find
       :desc "" "i" #'org-roam-node-insert))

(map! :map objed-map
      :desc "" "`" #'ff-find-other-file
      :desc "" "?" #'objed-show-top-level
      :desc "" "!" #'devdocs-lookup
      :desc "" "C-<home>" #'+private|modal|guide/toggle
      :desc "" "C-<prior>" #'+private|modal|guide/prev
      :desc "" "C-<next>" #'+private|modal|guide/next)

;; (when (and (display-graphic-p) IS-MAC)

;;   (map! :map prog-mode-map
;;       :desc "" "s-<up>" #'windmove-up
;;       :desc "" "s-<left>" #'windmove-left
;;       :desc "" "s-<down>" #'windmove-down
;;       :desc "" "s-<right>" #'windmove-right)

;;   (map! :map objed-mode-map
;;       :desc "" "s-<up>" #'windmove-up
;;       :desc "" "s-<left>" #'windmove-left
;;       :desc "" "s-<down>" #'windmove-down
;;       :desc "" "s-<right>" #'windmove-right)

;;   (map! :map org-mode-map
;;       :desc "" "s-<up>" #'windmove-up
;;       :desc "" "s-<left>" #'windmove-left
;;       :desc "" "s-<down>" #'windmove-down
;;       :desc "" "s-<right>" #'windmove-right))

;; (when (and (display-graphic-p) IS-LINUX)

;;   (map! :map prog-mode-map
;;       :desc "" "M-<up>" #'windmove-up
;;       :desc "" "M-<left>" #'windmove-left
;;       :desc "" "M-<down>" #'windmove-down
;;       :desc "" "M-<right>" #'windmove-right)

;;   (map! :map objed-mode-map
;;       :desc "" "M-<up>" #'windmove-up
;;       :desc "" "M-<left>" #'windmove-left
;;       :desc "" "M-<down>" #'windmove-down
;;       :desc "" "M-<right>" #'windmove-right)

(map! :map org-mode-map
      :desc "" "M-<up>" #'windmove-up
      :desc "" "M-<left>" #'windmove-left
      :desc "" "M-<down>" #'windmove-down
      :desc "" "M-<right>" #'windmove-right)

;;   (map! :map typescript-mode-map
;;       :desc "" "M-<up>" #'windmove-up
;;       :desc "" "M-<left>" #'windmove-left
;;       :desc "" "M-<down>" #'windmove-down
;;       :desc "" "M-<right>" #'windmove-right)

;;   (map! :map rjsx-mode-map
;;       :desc "" "M-<up>" #'windmove-up
;;       :desc "" "M-<left>" #'windmove-left
;;       :desc "" "M-<down>" #'windmove-down
;;       :desc "" "M-<right>" #'windmove-right)

;;   (map! :map css-mode-map
;;       :desc "" "M-<up>" #'windmove-up
;;       :desc "" "M-<left>" #'windmove-left
;;       :desc "" "M-<down>" #'windmove-down
;;       :desc "" "M-<right>" #'windmove-right))

(map! :map devdocs-mode-map
      :desc "" "!" #'devdocs-lookup)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|bindings)
