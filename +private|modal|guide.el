;;; +private|modal|helper.el -*- lexical-binding: t; no-byte-compile: t; -*-

(use-package! objed

  :config

  (require '+private|themes)
  (require '+private|faces)

  (require 'svg-lib)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (defvar +private|modal|guide--buffer nil "")
  (defvar +private|modal|guide--buffer-name "*Modal guide*" "")

  (defcustom +private|modal|guide--hook '()
    ""
    :group '+private|modal|guide
    :type 'hook)

  (defun +private|modal|guide--init-buffer ()
    ""
    (setq +private|modal|guide--buffer (get-buffer-create +private|modal|guide--buffer-name))
    (with-current-buffer +private|modal|guide--buffer
      (+private|modeline/set-guide-modeline))
    +private|modal|guide--buffer)

  (defun +private|modal|guide--redisplay (columns)
    (with-current-buffer +private|modal|guide--buffer
      (setq-local fill-column (- columns 15))
      (erase-buffer)
      (+private|modal|guide--init-buffer-contents)
      (beginning-of-buffer))
    (run-hooks '+private|modal|guide--hook))

  (defun +private|modal|guide--begin-section (text)
    (insert text)
    (put-text-property (point-at-bol) (point) 'face '+private|faces-section)
    (insert "\n")
    (insert "\n"))

  (defun +private|modal|guide--end-section ()
    (insert "\n"))

  (defun +private|modal|guide--insert-desc (text)
    (set-mark (point))
    (insert text)
    (put-text-property (point-at-bol) (point) 'face '+private|faces-hint)
    (fill-paragraph 'justify t))

  (defun +private|modal|guide--insert-keybindings (bindings)
    (mapcar
     (lambda (x)
       (progn
         (insert-image (svg-lib-tag x nil
                                    :foreground (face-attribute '+private|faces-keybinding :foreground)
                                    :background (face-attribute '+private|faces-keybinding :background)
                                    :font-family (face-attribute '+private|faces-keybinding :family)
                                    :font-weight 500 :stroke 0))
         (insert " ")))
     (split-string bindings "\\ ")))

  (defun +private|modal|guide--insert-filler ()
    (insert " ")
    (while (< (current-column) (+ fill-column 4))
      (insert "."))
    (insert " "))

  (defun +private|modal|guide--insert-entry (desc keys)
    (insert " ")
    (insert " ")
    (+private|modal|guide--insert-desc desc)
    (+private|modal|guide--insert-filler)
    (+private|modal|guide--insert-keybindings keys)
    (insert "\n")
    (insert "\n"))

  (defun +private|modal|guide--init-buffer-contents ()
    (+private|modal|guide--begin-section "Common emacs keys")
    (+private|modal|guide--insert-entry "Enter" "M-g")
    (+private|modal|guide--insert-entry "Leave" "C-g")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Activation commands")
    (+private|modal|guide--insert-entry "Basic" "M-SPC")
    (+private|modal|guide--insert-entry "Aced" "M-#")
    (+private|modal|guide--insert-entry "With to previous/next identifier" "C-+ C-.")
    (+private|modal|guide--insert-entry "Object at point (whole) and move to its start" "M-[")
    (+private|modal|guide--insert-entry "Object at point (whole) and move to its end" "M-]")
    (+private|modal|guide--insert-entry "Object at point (part) and move to its start" "M-(")
    (+private|modal|guide--insert-entry "Object at point (part) and move to its end" "M-)")
    ;; (+private|modal|guide--insert-entry "Activate objed, choosing initial object based on last-command and;;  objed-cmd-alist" "M-SPC")
    ;; (+private|modal|guide--insert-entry "Choose an object and activate with it" "M-#")
    ;; (+private|modal|guide--insert-entry "Move to previous/next identifier and activate identifier object" "C-+/C-.")
    ;; (+private|modal|guide--insert-entry "Activate object at point (determined from context) and move to its start" "M-[")
    ;; (+private|modal|guide--insert-entry "Activate object at point (determined from context) and move to its end" "M-]")
    ;; (+private|modal|guide--insert-entry "Move to beginning of object at point and active text moved over" "M-(")
    ;; (+private|modal|guide--insert-entry "Move to end of object at point and active text moved over" "M-)")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Basic movement commands")
    (+private|modal|guide--insert-entry "Move forward/backward one char" "l h")
    (+private|modal|guide--insert-entry "Move forward/backward one sexp" "f b")
    (+private|modal|guide--insert-entry "Move forward/backward one word" "s r")
    (+private|modal|guide--insert-entry "Move to the next/previous line" "n p")
    ;; (+private|modal|guide--insert-entry "Move forward/backward one char and activate the char object" "l h")
    ;; (+private|modal|guide--insert-entry "Move forward/backward one sexp and activate the sexp object" "f b")
    ;; (+private|modal|guide--insert-entry "Move forward/backward one word and activate the word object" "s r")
    ;; (+private|modal|guide--insert-entry "Move to the next/previous line and activate the line object" "n p")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Block objects commands")
    (+private|modal|guide--insert-entry "Activate (line based) object at point and move to its start. On repeat proceed to beginning of the indentation block, comment block, paragraph or other block objects" "v")
    (+private|modal|guide--insert-entry "Move to beginning of line and activate the text moved over. On repeat proceed to beginning of blocks like explained above" "a")
    (+private|modal|guide--insert-entry "Move to end of line and activate the text moved over. On repeat proceed to end of blocks like explained above" "e")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Context objects commands")
    (+private|modal|guide--insert-entry "Activate the inner part of the object at point and move to the start. This is useful to act on the content of the string, brackets and so on. On repeat expand to other objects around current position" "o")
    (+private|modal|guide--insert-entry "Move point to the other side of the current object" "j")
    (+private|modal|guide--insert-entry "Toggle object state. Switches between inner and whole object state" "t")
    (+private|modal|guide--insert-entry "Move to the start of previous/next instance of current object type" "[ ]")
    (+private|modal|guide--insert-entry "Move to inner beginning/end of the object at point and activate the text moved over" "( )")
    (+private|modal|guide--insert-entry "Move forward/backward paragraph and switch to paragraph object" "{ }")
    (+private|modal|guide--insert-entry "Goto first/last instance of current object type" "< >")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Switch commands")
    (+private|modal|guide--insert-entry "Switch to identifier object or move to next" ".")
    (+private|modal|guide--insert-entry "Switch to identifier object and move to previous" ",")
    (+private|modal|guide--insert-entry "Prefix to switch to other objects, see objed-object-map for available objects and objed-define-object to add your own" "c")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Indentation & Move commands")
    (+private|modal|guide--insert-entry "Move/indent all lines in object right/leftward" "C-left C-right")
    (+private|modal|guide--insert-entry "Move/indent all lines in object to right/leftward to tab stop" "M-left M-right")
    (+private|modal|guide--insert-entry "Slurp following sexp into current object/Barf last sexp out of current object" "C-M-left C-M-right")
    (+private|modal|guide--insert-entry "Move current object type forward/backward" "F B")
    (+private|modal|guide--insert-entry "Switch to char object and move it forward/backward" "L H")
    (+private|modal|guide--insert-entry "Switch to word object and move it forward/backward" "S R")
    (+private|modal|guide--insert-entry "Switch to line object and move it forward/backward" "N P")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Edition commands")
    (+private|modal|guide--insert-entry "Delete current object(s) and switch to insert" "i")
    (+private|modal|guide--insert-entry "Kill current object(s). Continues by selecting the next instance from point" "k")
    (+private|modal|guide--insert-entry "Kill current object. Continues by selecting the previous instance from point" "K")
    (+private|modal|guide--insert-entry "Delete current object(s). Continues by selecting the next instance from point" "d")
    (+private|modal|guide--insert-entry "Delete current object. Continues by selecting the previous instance from point" "D")
    (+private|modal|guide--insert-entry "Copy current object(s). On repeat move to next and append it to kill-ring" "w")
    (+private|modal|guide--insert-entry "Yank last killed text at point. On repeat, cycle through kill-ring" "y")
    (+private|modal|guide--insert-entry "Indent object(s)" "\\")
    (+private|modal|guide--insert-entry "Un/comment object(s)" ";")
    (+private|modal|guide--insert-entry "Query replace narrowed to current object" "%")
    (+private|modal|guide--insert-entry "Replace object with inner part (raise)" "^")
    (+private|modal|guide--insert-entry "Run object contents as shell commands" "!")
    (+private|modal|guide--insert-entry "Pipe object region through shell command" "&")
    (+private|modal|guide--insert-entry "Incrementally construct command chain to replace text" "|")
    (+private|modal|guide--insert-entry "Evaluate current object in REPL (need eval-in-repl to be installed)" "C-RET")
    (+private|modal|guide--insert-entry "Duplicate object" "M-RET")
    (+private|modal|guide--insert-entry "Comment and duplicate object" "S-RET")
    (+private|modal|guide--insert-entry "Insert new (empty) instance of current object type. This inserts the object boundaries without the inner content" "C-M-RET")
    (+private|modal|guide--insert-entry "Spell check textual content of object using flyspell" "$")
    (+private|modal|guide--insert-entry "Undo in current object region" "~")
    (+private|modal|guide--insert-entry "Add surroundings to object(s) with any pair using electric" "\"")
    (+private|modal|guide--insert-entry "Prefix to access other operations" "x")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Miscellaneous commands")
    (+private|modal|guide--insert-entry "Quit window or reformat in edit buffer" "d")
    (+private|modal|guide--insert-entry "Pop to last state, which restores the last position and any object data" "u")
    (+private|modal|guide--insert-entry "Choose an instance of current object type on the screen with avy" "z")
    (+private|modal|guide--insert-entry "Choose an instance of current object, by first lines content" "M-g o")
    (+private|modal|guide--insert-entry "Add/Remove current object to marked objects and move to next" "m")
    (+private|modal|guide--insert-entry "Add/Remove current object to marked objects and move to previous" "M")
    (+private|modal|guide--insert-entry "Activate region with current object (extend current object)" "@")
    (+private|modal|guide--insert-entry "Set mark" "C-Space")
    (+private|modal|guide--insert-entry "Undo last edit command" "/")
    (+private|modal|guide--insert-entry "Exit and deactivate objed" "g")
    (+private|modal|guide--end-section)

    (+private|modal|guide--begin-section "Dispatch keys")
    (+private|modal|guide--insert-entry "Mark more instances of current object inside defun/buffer" "*")
    (+private|modal|guide--insert-entry "Switch to another object using avy" "#")
    (+private|modal|guide--insert-entry "Switch to another object inside the current one using avy" "=")
    (+private|modal|guide--insert-entry "Activate part from point forward until boundary of some object" "`")
    (+private|modal|guide--insert-entry "Activate part from point backward until boundary of some object" "´")
    (+private|modal|guide--insert-entry "Extend current object by including trailing whitespace" "+")
    (+private|modal|guide--end-section))

  (defun +private|modal|guide/prev ()
    (interactive)
    (when (buffer-live-p +private|modal|guide--buffer)
      (setq other-window-scroll-buffer +private|modal|guide--buffer)
      (scroll-other-window-down)))

  (defun +private|modal|guide/next ()
    (interactive)
    (when (buffer-live-p +private|modal|guide--buffer)
      (setq other-window-scroll-buffer +private|modal|guide--buffer)
      (scroll-other-window)))

  (set-popup-rule! +private|modal|guide--buffer-name :side 'right :size 0.33 :select nil :quit t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (defcustom +private|modal|guide-idle-time 5
    ""
    :group '+private|modal|guide
    :type 'float)

  (defvar +private|modal|guide-global-timer nil)

  (defun +private|modal|guide/toggle ()
    (interactive)
    (if (and +private|modal|guide--buffer (window-live-p (get-buffer-window +private|modal|guide--buffer)))
        (progn
          (+private|modal|guide/leave)
          (cancel-timer +private|modal|guide-global-timer))
      (progn
        (setq +private|modal|guide-global-timer
              (run-with-idle-timer +private|modal|guide-idle-time
                                   :repeat #'+private|modal|guide/enter))
        (+private|modal|guide/enter))))

  (defun +private|modal|guide/leave ()
    (when-let ((buffer +private|modal|guide--buffer)
               (window (get-buffer-window +private|modal|guide--buffer-name)))
      (+popup/close window)))

  (defun +private|modal|guide/enter ()
    (when doom-modeline--objed-active
      (unless (and +private|modal|guide--buffer (window-live-p (get-buffer-window +private|modal|guide--buffer)))
        (display-buffer (+private|modal|guide--init-buffer))
        (+private|modal|guide--redisplay (window-total-width (get-buffer-window +private|modal|guide--buffer))))
      (message "Paginate with C-<prior>|C-<next>")))

  (define-minor-mode +private|modal|guide-mode
    ""
    :group '+private|modal|guide
    (if +private|modal|guide-mode
        (unless +private|modal|guide-global-timer
          (setq +private|modal|guide-global-timer
                (run-with-idle-timer +private|modal|guide-idle-time
                                     :repeat #'+private|modal|guide/enter)))))

  (add-hook! 'objed-init-hook #'+private|modal|guide-mode)
  (add-hook! 'objed-exit-hook #'+private|modal|guide/leave))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|modal|guide)
