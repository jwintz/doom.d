;;; +private|core.el -*- lexical-binding: t; -*-

(defconst IS-MACM1
  (and IS-MAC (string= (shell-command-to-string "uname -p") "arm\n")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|core)
