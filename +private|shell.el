;;; +private|shell.el -*- lexical-binding: t; -*-

(setq eshell-banner-message "\n")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (defun +private|shell/font-face ()
;;   (setq buffer-face-mode-face '(:family "MonoLisa"))
;;   (buffer-face-mode)
;;   (text-scale-decrease 1))

;; (add-hook!  'vterm-mode-hook #'+private|shell/font-face)
;; (add-hook! 'eshell-mode-hook #'+private|shell/font-face)

(set-popup-rule! "*doom:vterm-popup:main*" :side 'bottom :size 0.25 :select t :quit nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|shell)
