;;; +private|ui.el -*- lexical-binding: t; -*-

(setq frame-title-format '("Emacs"))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cursor
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(when (display-graphic-p)
  (setq-default cursor-type '(bar . 2)))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Display line numbers
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq display-line-numbers-type t)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use the forked kaolin stack
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq private-themes-bold t
      private-themes-italic t
      private-themes-italic-comments t
      private-themes-underline t
      private-themes-modeline-border nil);

(when (and (display-graphic-p) IS-MAC)
  (setq doom-theme '+private|theme-eclipse))

(when (and (display-graphic-p) IS-LINUX)
  (setq doom-theme '+private|theme-fluent-blue))

(when (display-graphic-p)
  (private-treemacs-theme))

(when (display-graphic-p)
  (solaire-global-mode +1))

(when (display-graphic-p)
  (load-theme doom-theme t))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Display color codes
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package rainbow-mode
  :hook (prog-mode . rainbow-mode))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ligatures test
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; |||> <||| <==> <!-- #### ~~> *** ||= ||> ::: ::= =:= === ==> =!= =>> =<< =/=
;; !!. >=> >>= >>> >>- >-> ->> --> --- -<< <~~ <~> <*> <|| <|> <$> <== <=> <=<
;; <-- <-< <<= <<- <<< <+> </> ### #_( ..< ... +++ /== /// _|_ www && ^= ~~ ~@
;; ~> ~- ** *> */ || |} |] |= |> |- {| [| ]# :: := :> :< $> == => != !! >: \\\\
;; >= >> >- -~ -| -> -- -< <~ <* <| <: <$ <= <> <- << <+ </ #{ #[ #: #= #!  !==
;; ## #( #? #_ %% .= .- .. .? +> ++ ?: ?= ?. ?? ;; /* /= /> // __ ~~ (* *) <->

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fancy splash
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq fancy-splash-image (expand-file-name "~/.doom.d/+private|banner-icon.png"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'solaire-mode-themes-to-face-swap '+private|theme-light)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|ux)
