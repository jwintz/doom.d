;;; private-zorin-blue-dark-theme.el --- Zorin-Blue-Dark Kaolin theme variant
;;; Commentary:

;;; Code:
(require '+private|themes)

(defgroup private-zorin-blue-dark nil
  "Kaolin zorin-blue-dark theme options."
  :group 'private-themes)

(defcustom private-zorin-blue-dark-alt-bg nil
  "Use alternative darker background color."
  :type 'boolean
  :group 'private-zorin-blue-dark)


(define-private-theme zorin-blue-dark "Dark purple Kaolin theme variant."
  ;; Palette modification
  (
   (azure2 "#325074")
   (purple3 "#C68EDE")

   (bg0 (if private-zorin-blue-dark-alt-bg "#171d20" "#171d20") black0)
   (bg1 (if private-zorin-blue-dark-alt-bg "#1e2529" "#1e2529") black1)
   (bg2 (if private-zorin-blue-dark-alt-bg "#242d32" "#242d32") black2)
   (bg3 (if private-zorin-blue-dark-alt-bg "#28414e" "#28414e") black3)
   (bg4 (if private-zorin-blue-dark-alt-bg "#304955" "#304955") black4)

   (fg1 "#9cbfd0")

   (private-black   bg1)
   (private-red     red3)
   (private-green   spring-green1)
   (private-yellow  orange1)
   (private-blue    azure3)
   (private-magenta violet3)
   (private-cyan    cyan3)
   (private-white   fg1)

   (comment     gray3)
   (comment-alt azure4)
   (comment-contrast cerulean7)

   (keyword     cerulean4)
   (metakey     (if private-themes-distinct-metakeys ultramarine3 comment)) ; todo
   (builtin     azure3)
   ;; (functions   builtin)
   (functions   cyan3)
   (var         violet3)
   (const       magenta3)
   (type        pink1)
   (prep        ultramarine3)
   (num         amber3)
   (bool        num)

   (keysym amber3)

   (str         green3)
   (str-alt     spring-green4)
   (doc         str-alt)
   (warning     orange1)
   (err         red3)

   (dim-buffer "#0F0F17")
   (hl         aquamarine0)
   (hl-line    bg2)
   ;; ; (hl-indent bg4)
   ;; (selection  bg4)
   (selection  aquamarine6)
   (pulse      bg4)

   (todo pink1)

   (tooltip-hl-bg bg4)

   (search1 vermilion3)
   (search2 teal0)
   (search3 yellow3)

   (rb1 blue4)
   (rb2 violet4)
   (rb3 teal1)
   (rb4 crimson4)
   (rb5 azure4)
   (rb6 spring-green4)
   (rb7 vermilion4)
   (rb8 capri4)
   (rb9 azure3)

   (diff-add spring-green1)
   (diff-mod orange1)
   (diff-rem red3)

   ;; Mode-line
   (line-fg           fg4)
   (line-bg1          bg2)
   (line-bg2          bg3)
   (line-border       (if private-themes-modeline-border bg3 line-bg1))

   (segment-active    gray3)
   (segment-inactive  gray3)

   (win-border    bg3)
   (line-num-fg   gray3)
   (line-num-hl   keyword))

  (
   (highlight-quoted-quote   (:foreground functions))
   (highlight-quoted-symbol  (:foreground amber3))

   (org-document-title  (:foreground str))
   (org-code            (:foreground pink1))
   (org-verbatim        (:foreground spring-green1))
   (org-level-2         (:foreground functions))

   (git-gutter:added    (:background diff-add :foreground diff-add))
   (git-gutter:modified (:background diff-mod :foreground diff-mod))
   (git-gutter:deleted  (:background diff-rem :foreground diff-rem))
   )

  ;; Set custom vars
  (when private-themes-git-gutter-solid
    (custom-theme-set-faces
     'private-ocean
     `(git-gutter:added     ((t (:background ,diff-add :foreground ,diff-add))))
     `(git-gutter:modified  ((t (:background ,diff-mod :foreground ,diff-mod))))
     `(git-gutter:deleted   ((t (:background ,diff-rem :foreground ,diff-rem)))))))

;;; private-zorin-blue-dark-theme.el ends here
