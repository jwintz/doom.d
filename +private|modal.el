;;; +private|modal.el -*- lexical-binding: t; no-byte-compile: t -*-

(use-package! objed

  :after-call pre-command-hook

  :config

  (pushnew! objed-keeper-commands 'undo-fu-only-undo 'undo-fu-only-redo)

  (defvar +objed--extra-face-remaps nil)

  (defadvice! +objed--add-face-remaps-a (&rest _)
    "Add extra face remaps when objed activates."
    :after 'objed--init
    (when (memq 'objed-hl (assq 'hl-line face-remapping-alist))
      (push (face-remap-add-relative 'solaire-hl-line-face 'objed-hl)
            +objed--extra-face-remaps)))

  (defadvice! +objed--remove-face-remaps-a (&rest _)
    "Remove extra face remaps when objed de-activates."
    :after 'objed--reset
    (unless (memq 'objed-hl (assq 'hl-line face-remapping-alist))
      (dolist (remap +objed--extra-face-remaps)
        (face-remap-remove-relative remap))
      (setq +objed--extra-face-remaps nil)))

  (objed-mode +1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (defun +private/modal-enter-maybe ()
    (if (derived-mode-p 'prog-mode) (objed-init)))

  (defun +private/modal-enter ()
    (setq-default cursor-type 'box)
    (setq objed-cursor-color (face-attribute '+private|faces-objed-mode :foreground))
    (setq objed--saved-cursor (face-attribute '+private|faces-objed-mode :foreground))
    (set-cursor-color (face-attribute '+private|faces-objed-mode :foreground)))

  (defun +private/modal-leave ()
    (setq-default cursor-type '(bar . 2))
    (set-cursor-color (face-attribute '+private|faces-emacs-mode :foreground)))

  (add-hook! 'objed-init-hook #'+private/modal-enter)
  (add-hook! 'objed-exit-hook #'+private/modal-leave)

  (add-hook!          'prog-mode-hook #'+private/modal-enter-maybe)
  (add-hook! 'windmove-post-move-hook #'+private/modal-enter-maybe))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|modal)
