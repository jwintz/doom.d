;;; +private|linting.el -*- lexical-binding: t; -*-

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TreeSitter
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (unless IS-MACM1 (use-package! tree-sitter
;;   :defer t ;; loading is handled by individual modes
;;   :hook (tree-sitter-after-on . tree-sitter-hl-mode)
;;   :config
;;   ;; This makes every node a link to a section of code
;;   (setq tree-sitter-debug-jump-buttons t
;;         ;; and this highlights the entire sub tree in your code
;;         tree-sitter-debug-highlight-jump-region t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|linting)
