;;; +private|lookup.el -*- lexical-binding: t; -*-

(use-package! devdocs
  :config

  (defun +private|lookup/post-display ()
    (when-let (buffer (get-buffer "*devdocs*"))
      (with-current-buffer buffer
        (setq-local mode-line-format nil)
        (setq-local header-line-format nil)
        (+private|modeline/set-documentation-modeline))))

  (add-hook! '+popup-buffer-mode-hook #'+private|lookup/post-display)

  (add-hook         'cc-mode-hook #'(lambda () (setq-local devdocs-current-docs '("qt~6.1"))))
  (add-hook       'rust-mode-hook #'(lambda () (setq-local devdocs-current-docs '("rust"))))
  (add-hook 'emacs-lisp-mode-hook #'(lambda () (setq-local devdocs-current-docs '("elisp"))))
  (add-hook      'cmake-mode-hook #'(lambda () (setq-local devdocs-current-docs '("cmake~3.21"))))

  ;; (add-hook 'devdocs-mode-hook #'(lambda ()
  ;;       (face-remap-add-relative 'variable-pitch :family "MonoLisa" :height 0.9)))

  (set-popup-rule! "*devdocs*" :side 'bottom :size 0.5 :select t :quit nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|lookup)
