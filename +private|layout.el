;;; +private|layout.el -*- lexical-binding: t; -*-

(defcustom windmove-pre-move-hook nil
  "Hook run before windmove select is triggered."
  :group 'windmove
  :type 'hook)

(defcustom windmove-post-move-hook nil
  "Hook run after windmove select is triggered."
  :group 'windmove
  :type 'hook)

(defadvice windmove-do-window-select (around my-windmove-do-window-select act)
  (run-hooks 'windmove-pre-move-hook)
  ad-do-it
  (run-hooks 'windmove-post-move-hook))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '+private|layout)
